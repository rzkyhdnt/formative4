import java.util.ArrayList;
import java.util.Scanner;

public class Bioskop {
    private String name;
    public String filmName, jamTayang;
    boolean confirm = true;

    Scanner scan = new Scanner(System.in);
    int customer, studio;
    ArrayList<String> customerSeat = new ArrayList<String>();

    public void getInfo(String name){
        this.name = name;
        System.out.println("-------------------------------------------------------------------------");
        System.out.println("                   SELAMAT DATANG DI " + name.toUpperCase() + "          ");
        System.out.println("-------------------------------------------------------------------------");
        System.out.println("                              NOW SHOWING                                ");
        System.out.println("1. Avengers - End Game      Studio 5        12.30   15.00   17.00   19.00");
        System.out.println("2. Demon Slayer             Studio 4        15.00   17.00   18.30   21.00");
        System.out.println("3. Mortal Kombat            Studio 3        12.30   17.00   21.00        ");
        System.out.println("4. Chucky 3                 Studio 2        19.00   21.30   23.00        ");
        System.out.println("5. Rambo 11                 Studio 1        15.00   19.00   21.00        ");
    }
}

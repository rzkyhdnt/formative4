public class Film extends Bioskop {
    public void getFilm() {
        System.out.println(super.filmName);
    }

    public void getJamTayang(){
        System.out.println(jamTayang);
    }

    public String setFilm() {
        int pilihFilm = scan.nextInt();

        switch (pilihFilm) {
            case 1:
                super.filmName = "Avengers - Endgame";
                break;
            case 2:
                super.filmName = "Demon Slayer";
                break;
            case 3:
                super.filmName = "Mortal Kombat";
                break;
            case 4:
                super.filmName = "Chucky 3";
                break;
            case 5:
                super.filmName = "Rambo 11";
                break;
            default:
                System.out.println("Film yang anda pilih tidak tersedia");
                break;
        }
        return super.filmName;
    }

    public void setJamTayang(){
        if(filmName == "Avengers - Endgame"){
            System.out.println("1. 12.30");
            System.out.println("2. 15.00");
            System.out.println("3. 17.00");
            System.out.println("4. 19.00");
            System.out.print("Pilihan : ");
            int pilihJamTayang = scan.nextInt();

            switch (pilihJamTayang){
                case 1 : jamTayang = "12.30"; studio = 5; break;
                case 2 : jamTayang = "15.00"; studio = 5; break;
                case 3 : jamTayang = "17.00"; studio = 5; break;
                case 4 : jamTayang = "19.00"; studio = 5; break;
                default: jamTayang = null;
            }
        } else if(filmName == "Demon Slayer"){
            System.out.println("1. 15.00");
            System.out.println("2. 17.00");
            System.out.println("3. 18.30");
            System.out.println("4. 21.00");
            System.out.print("Pilihan : ");
            int pilihJamTayang = scan.nextInt();

            switch (pilihJamTayang){
                case 1 : super.jamTayang = "15.00"; studio = 4; break;
                case 2 : super.jamTayang = "17.00"; studio = 4; break;
                case 3 : super.jamTayang = "18.30"; studio = 4; break;
                case 4 : super.jamTayang = "21.00"; studio = 4; break;
                default: super.jamTayang = null;
            }

        } else if(filmName == "Mortal Kombat"){
            System.out.println("1. 12.30");
            System.out.println("2. 17.00");
            System.out.println("3. 21.00");
            System.out.print("Pilihan : ");
            int pilihJamTayang = scan.nextInt();

            switch (pilihJamTayang){
                case 1 : super.jamTayang = "12.30"; studio = 3; break;
                case 2 : super.jamTayang = "17.00"; studio = 3; break;
                case 3 : super.jamTayang = "21.00"; studio = 3; break;
                default: super.jamTayang = null;
            }

        } else if(filmName == "Chucky 3"){
            System.out.println("1. 19.00");
            System.out.println("2. 21.30");
            System.out.println("3. 23.00");
            System.out.print("Pilihan : ");
            int pilihJamTayang = scan.nextInt();

            switch (pilihJamTayang){
                case 1 : super.jamTayang = "19.00"; studio = 2; break;
                case 2 : super.jamTayang = "21.30"; studio = 2; break;
                case 3 : super.jamTayang = "23.00"; studio = 2; break;
                default: super.jamTayang = null;
            }

        } else if(filmName == "Rambo 11"){
            System.out.println("1. 15.00");
            System.out.println("2. 19.00");
            System.out.println("3. 21.00");
            System.out.print("Pilihan : ");
            int pilihJamTayang = scan.nextInt();

            switch (pilihJamTayang){
                case 1 : super.jamTayang = "15.00"; studio = 1; break;
                case 2 : super.jamTayang = "19.00"; studio = 1; break;
                case 3 : super.jamTayang = "21.00"; studio = 1; break;
                default: super.jamTayang = null;
            }
        }
    }
}

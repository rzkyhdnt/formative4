import java.lang.reflect.Array;
import java.util.ArrayList;

public class Studio extends Bioskop {
    int maxSeats = 300;
    String temp;
    String[][] availableSeats = {
            {"E1", "E2", "E3", "E4", "E5", "E6"},
            {"F1", "F3", "F6"},
            {"G4", "G6"}
    };

    public int setCustomer(){
        customer = scan.nextInt();
        return customer;
    }

    public void setCustomerSeat(){
        System.out.println("Tersedia");

        for(int i = 0; i < availableSeats.length; i++){
            for(int j = 0; j < availableSeats[i].length; j++){
                System.out.print(availableSeats[i][j] + " ");
            }
        }

        System.out.println("\nPilihan : ");

        for(int k = 0; k < customer; k++){
            temp = scan.next();
            customerSeat.add(temp);
        }
    }

    public boolean confirm(){
        String conf = scan.next();
        try {
            if(conf.equals("Y")){
                confirm = false;
            } else if(conf.equals("N")) {
                confirm = true;
            }
        } catch(Exception e){
            System.out.println(e.getMessage());
        }
        return confirm;
    }
}
